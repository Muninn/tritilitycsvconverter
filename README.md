# Tritility CSVConverter for Lumen

This CSV convertor is activated on the CLI inside your 
Lumen project by running artisan with the required parameters.

It converts a CSV format such as:

To the following format:


## Installation

Installation is a simple case of executing composer:

```
composer require  OdinsHat/trility_csv_converter
```

## Setup

Add this service provider to your bootstrap/app.php file in Lumen.

```php
$app->register(Tritility\CsvConverter\Providers\TritilityServiceProvider::class);
```
Then the commans will be available to use. You can also view them by issuing the command:

```
artisan
```

On your terminal to see the command name under tritility and its description.


## Operation

Run the following command after instllation:

```
tritility:convertcsv --in=input.csv --out=output.csv
```

Running this on a file with the below format will convert it into the required format.

The first column shows the table headers of the expected input CSV and the 
second column shows the headers of the output CSV. The third column shows the 
output data type and format

| Spreadsheet              | Column 0\-indexed | Output CSV Columns   | Datatype               |
|--------------------------|-------------------|----------------------|------------------------|
| Company Name             | 0                 | company\_name        | String lowercase       |
| Company Number           | 1                 | company\_number      | String                 |
| Address 1                | 2                 | address\_line\_1     | String Uppercase Words |
| Address 2                | 3                 | address\_line\_2     | String Uppercase Words |
| Address 3                | 4                 | town                 | String Uppercase Words |
| Address 4                | 5                 | county               | String Uppercase Words |
| Post Code                | 6                 | postcode             | String Uppercase       |
| Telephone Number         | 8                 | telephone            | Number                 |
| SIC Code and Description | 20                | sic\_code            | Number                 |
| Director Forename        | 24                | contact\_first\_name | String Uppercase Words |
| Director Surname         | 25                | contact\_last\_name  | String Uppercase Words |


## Todos

Potential possible extnsions could be made to this code 
including but not limited to:

* Geocoding post code to get proper town & county column
* PHPUnit testing coverage above 90%