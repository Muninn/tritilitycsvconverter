<?php
/**
 * Tritility CSV Converter is an Artisan console command
 * for making it possible to open a CSV of a certain
 * format then export as another set format. With some minor
 * formatting changes and movement of columns depedent on
 * others statuses.
 *
 * @category LumenConsoleCommand
 * @package  Tritility\Console\CsvConverter
 * @author   Doug Bromley <doug@tintophat.com>
 * @license  GPL https://gitlab.com/Muninn/tritilitycsvconverter/-/blob/master/LICENSE
 * @link     https://laravel.com/docs/7.x/artisan#writing-commands
 * @link     https://lumen.laravel.com/docs/7.x
 */

namespace Tritility\CsvConverter;

use Illuminate\Console\Command;
use Exception;
use Log;

/**
 * Command class accessible by artisan on the CLI to convert
 * a given CSV file into a preffered format of CSV.
 *
 * @category Tritility
 * @package  TritilityCsvConverter
 * @author   Doug Bromley <doug@tintophat.com>
 * @license  GPL
 * @link     https://laravel.com/docs/7.x/artisan#writing-commands
 */
class TritilityCsvConverterCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tritility:convertcsv 
                            --in={input.csv} 
                            --out={output.csv}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Converts given CSV file to fixed format';

    /**
     * Source CSV filename provided at artisan command line.
     *
     * @var string
     */
    protected $sourceFile = '';

    /**
     * Output CSV file provided at Artisan command line
     *
     * @var string
     */
    protected $outputFile = '';

    /**
     * Extracted assoc array of rows and columns for putting into new CSV file
     *
     * @var array
     */
    protected $extracted = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->validateOptions();

            $this->sourceFile = $this->option('in');
            $this->$outputFile = $this->option('out');

            $this->extracted = $this->extractCsvFile();
            $this->createCsvFile();
        } catch (Exception $e) {
            $errorMessage = sprintf(
                'The console command tritility:convertcsv failed with error: %s',
                $e->getMessage()
            );
            Log::error($errorMessage);
            $this->error($errorMessage);
        }
        
    }


    /**
     * Validate the options passed to the command on the CLI.
     *
     * Checks not only that the options are given but also that
     * the input & output file exist otherwise throws an exception.
     *
     * @throws RuntimeException       when either argument is missing
     * @throws FileNotFoundException  when the input file is not found
     *
     * @return boolean
     */
    protected function validateOptions() : bool
    {
        if ($this->option('inputfile') !== null && $this->option('outputfile') !== null) {
            $this->warn(
                'Please pass in both the input file and output file in the format
                tritility:convertcsv --in=input.csv --out=output.csv'
            );
            throw new \RuntimeException(
                'You need to include both an input 
                and output file to convert the CSV'
            );
        }

        if (!file_exists($this->option('inputfile'))) {
            throw new \FileNotFoundException(
                'The input file provided does not exist.'
            );
        }

        return true;
    }

    /**
     * Extract contents of input CSV file into array then output to CSV
     *
     * If there's no address3 and address4 then place address2
     * into county field (address4). Otherwise put contents in as
     * expected.
     *
     * @throws Exception    When input file can't be opened.
     *
     * @todo geocode the postcode field to get the county 
     *       using postcode.io or Google Geocoding API.
     *
     * @return array|mixed
     */
    protected function extractCsvFile()
    {
        $arrayOfRequiredRows = [];
        $arrayRow = [];

        if (($fp = fopen($this->option('in'), 'r')) !== false) {
            while (($row = fgetcsv($fp) !== false)) {
                $arrayRow = [];
                if (strlen($row[4]) > 0 && strlen($row[5] > 0)) {
                    $arrayRow = [
                        $row[0],
                        $row[1],
                        $row[2],
                        $row[3],
                        '""',
                        '""',
                        $row[4],
                        $row[8],
                        getSicCode($row[20]),
                        $row[24],
                        $row[25]
                    ];
                } else {
                    $arrayRow = [
                        $row[0],
                        $row[1],
                        $row[2],
                        $row[3],
                        $row[4],
                        $row[5],
                        $row[6],
                        $row[8],
                        getSicCode($row[20]),
                        $row[24],
                        $row[25]
                    ];
                }
                $arrayOfRequiredRows[] = $arrayRow;
            }
            fclose($fp);
            return $arrayOfRequiredRows;
        }
        throw new \Exception('Input file could not be opened.');

        return false;
    }

    /**
     * Create the new CSV file with the given output file name
     *
     * @return void
     */
    protected function createCsvFile()
    {
        $csvFileContents = $this->extracted;

        $fp = fopen($this->outputFile, 'w');

        foreach ($csvFileContents as $row) {
            fputcsv($fp, $row);
        }

        fclose($fp);
    }

    /**
     * Get the SIC code from a field containing it and the description
     *
     * Field comes in the format <pre>XXXXX - text description</pre>
     * A simple preg_match is used to catch the numbers and return
     * them for entry into the array to be written to file.
     *
     * @param string $sicCodeAndDescription the SIC code and description
     *
     * @return string
     */
    private function getSicCode($sicCodeAndDescription)
    {
        preg_match_all("/\d+/", $sicCodeAndDescription, $matches);
        return $matches[0];
    }
}
