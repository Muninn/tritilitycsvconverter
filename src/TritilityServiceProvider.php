<?php
/**
 * This is the service provider allowing the command to be
 * registered with the Lumen Artisan command.
 *
 * @category Tritility
 * @package  TritilityCsvConverter
 * @author   Doug Bromley <doug@tintophat.com>
 * @license  GPL
 * @link     https://lumen.laravel.com/docs/7.x/providers
 */

namespace Tritility\CsvConverter;

use Illuminate\Support\ServiceProvider;
use Tritility\CsvConverter\Console\TritilityCsvConverterCommand;

class TritilityServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerVendorPublishCommand();

        $this->commands(['command.tritility.convertcsv']);
    }

    /**
     * Register the Artisan command
     *
     * @return void
     */
    protected function registerTritilityCsvConverterCommand()
    {
        $this->app->singleton('command.tritility.convertcsv', function ($app) {
            return new TritilityCsvConverterCommand();
        });
    }

    /**
     * Get the services provided by the provider
     *
     * i.e. the Artisan command
     *
     *
     * @return array
     */
    public function provides()
    {
        return ['command.tritility.convertcsv'];
    }
}
